package Functional;

public interface Functional<T> {
    double compute(T function);
}
