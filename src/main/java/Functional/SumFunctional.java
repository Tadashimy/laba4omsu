package Functional;

import Functional.Functional;

import java.util.function.Function;

import RealFunction.RealFunction;

public class SumFunctional<T extends RealFunction> implements Functional<T> {
    private final double start;
    private final double end;

    public SumFunctional(double start, double end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public double compute(T function) {
        double mid = (start + end) / 2;
        return function.calculate(start) + function.calculate(end) + function.calculate(mid);
    }
}
