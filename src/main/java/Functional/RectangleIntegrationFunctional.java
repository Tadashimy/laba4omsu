package Functional;

import Functional.Functional;
import RealFunction.RealFunction;

import java.util.function.Function;

public class RectangleIntegrationFunctional<T extends RealFunction> implements Functional<T> {
    private final double start;
    private final double end;

    public RectangleIntegrationFunctional(double start, double end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public double compute(T function) {
        double startValue = function.calculate(start);
        double endValue = function.calculate(end);

        if (Double.isNaN(startValue) || Double.isNaN(endValue)) {
            throw new IllegalArgumentException("Область определения функции не содержится в [" + start + "; " + end + "]");
        }

        double result = 0;
        double step = (end - start) / 1_000;
        for (double i = start; i < end; i += step) {
            result += function.calculate(i) * step;
        }
        return result;
    }
}