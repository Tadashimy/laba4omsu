package RealFunction;

import RealFunction.RealFunction;

public class SinFunction implements RealFunction {
    private double A;
    private double B;
    private double lowerBound;
    private double upperBound;

    public SinFunction(double A, double B, double lowerBound, double upperBound) {
        this.A = A;
        this.B = B;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    @Override
    public double calculate(double x) {
        if ( x < lowerBound || x > upperBound) {
            throw new IllegalArgumentException("Область определения функции не содержится в [" + lowerBound + "; " + upperBound + "]");
        }return A * Math.sin(B * x);
    }

    @Override
    public double getLowerBound() {
        return lowerBound;
    }

    @Override
    public double getUpperBound() {
        return upperBound;
    }
}

