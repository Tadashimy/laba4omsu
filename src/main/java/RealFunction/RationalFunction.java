package RealFunction;

import RealFunction.RealFunction;

public class RationalFunction implements RealFunction {
    private double A;
    private double B;
    private double C;
    private double D;
    private double lowerBound;
    private double upperBound;

    public RationalFunction(double A, double B, double C, double D, double lowerBound, double upperBound) {
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    @Override
    public double calculate(double x) {
        if ( x < lowerBound || x > upperBound) {
            throw new IllegalArgumentException("Область определения функции не содержится в [" + lowerBound + "; " + upperBound + "]");
        }
        double denominator = C * x + D;
        if (denominator == 0) {
            throw new ArithmeticException("На ноль делить нельзя");
        }
        return (A * x + B) / denominator;
    }

    @Override
    public double getLowerBound() {
        return lowerBound;
    }

    @Override
    public double getUpperBound() {
        return upperBound;
    }
}

