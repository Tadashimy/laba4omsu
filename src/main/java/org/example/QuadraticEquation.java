package org.example;

public class QuadraticEquation {
    private final double a;
    private final double b;
    private final double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double[] solve() {
        if (a == 0 && b == 0) {return new double[0];}
        if (a != 0) {
            double discriminant = b * b - 4 * a * c;
            if (discriminant > 0) {
                double root1 = (-b + Math.sqrt(discriminant)) / (2 * a);
                double root2 = (-b - Math.sqrt(discriminant)) / (2 * a);
                return new double[]{root1, root2};
            } else if (discriminant == 0) {
                double root = -b / (2 * a);
                return new double[]{root};
            } else {
                return new double[0];
            }
        }
        else {
            return new double[]{c/-b};
        }
    }
}