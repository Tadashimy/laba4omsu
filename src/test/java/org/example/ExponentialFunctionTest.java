package org.example;

import RealFunction.ExponentialFunction;
import RealFunction.LinearFunction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExponentialFunctionTest {
    @Test
    public void testCase1() {
        ExponentialFunction exponentialFunction = new ExponentialFunction(2, 3, -5, 5);

        assertEquals(5, exponentialFunction.calculate(0), 0.0001);
        assertEquals(8.43656, exponentialFunction.calculate(1), 0.0001);
        assertEquals(3.7357, exponentialFunction.calculate(-1), 0.0001);
    }

    @Test
    public void testCase2() {
        ExponentialFunction exponentialFunction = new ExponentialFunction(1, 1, -2, 2);

        assertEquals(1.36788, exponentialFunction.calculate(-1), 0.0001);
        assertEquals(2, exponentialFunction.calculate(0), 0.0001);
        assertEquals(3.71828, exponentialFunction.calculate(1), 0.0001);
    }

    @Test
    public void testCase3() {
        ExponentialFunction exponentialFunction = new ExponentialFunction(2, 3, -5, 5);

        assertEquals(5, exponentialFunction.calculate(0), 0.0001);
        assertEquals(299.8263, exponentialFunction.calculate(5), 0.0001);
        assertEquals(3.0134, exponentialFunction.calculate(-5), 0.0001);
    }

    @Test
    public void testECase4() {
        ExponentialFunction exponentialFunction = new ExponentialFunction(0, 3, -5, 5);

        assertEquals(3, exponentialFunction.calculate(2), 0.0001);
    }
    @Test
    public void tastCase5() {
        ExponentialFunction exponentialFunction = new ExponentialFunction(3, 1, 0, 1);

        assertThrows(IllegalArgumentException.class, () -> {exponentialFunction.calculate(5);});
    }
}