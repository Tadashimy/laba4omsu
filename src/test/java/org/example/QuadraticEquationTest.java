package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuadraticEquationTest {
    @Test
    public void testCase1() {
        QuadraticEquation equation = new QuadraticEquation(0, 0, 0);
        double[] expectedRoots = {};
        assertArrayEquals(expectedRoots, equation.solve(), 0.0001);
    }
    @Test
    public void testCase2() {
        QuadraticEquation equation = new QuadraticEquation(0, 0, 1);
        double[] expectedRoots = {};
        assertArrayEquals(expectedRoots, equation.solve(), 0.0001);
    }

    @Test
    public void testCase3() {
        QuadraticEquation equation = new QuadraticEquation(1, 1, 0);
        double[] expectedRoots = {0.0, -1.0};
        assertArrayEquals(expectedRoots, equation.solve(), 0.0001);
    }

    @Test
    public void testCase4() {
        QuadraticEquation equation = new QuadraticEquation(0, 1, 1);
        double[] expectedRoots = {-1.0};
        assertArrayEquals(expectedRoots, equation.solve(), 0.0001);
    }

    @Test
    public void testCase5() {
        QuadraticEquation equation = new QuadraticEquation(1, 1, 1);
        double[] expectedRoots = {};
        assertArrayEquals(expectedRoots, equation.solve(), 0.0001);
    }

    @Test
    public void testCase6() {
        QuadraticEquation equation = new QuadraticEquation(0, 1, 0);
        double[] expectedRoots = {0.0};
        assertArrayEquals(expectedRoots, equation.solve(), 0.0001);
    }

    @Test
    public void testCase7() {
        QuadraticEquation equation = new QuadraticEquation(1, 0, 1);
        double[] expectedRoots = {};
        assertArrayEquals(expectedRoots, equation.solve(), 0.0001);
    }

    @Test
    public void testCase8() {
        QuadraticEquation equation = new QuadraticEquation(1, 0, 0);
        double[] expectedRoots = {0.0};
        assertArrayEquals(expectedRoots, equation.solve(), 0.0001);
    }
}