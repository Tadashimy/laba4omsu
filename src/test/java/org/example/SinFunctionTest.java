package org.example;

import RealFunction.SinFunction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SinFunctionTest {

    @Test
    public void testCase1() {
        SinFunction sineFunction = new SinFunction(2, 1, 0, Math.PI);

        assertEquals(2, sineFunction.calculate(Math.PI / 2), 0.0001);
        assertEquals(-2, sineFunction.calculate(3 * Math.PI / 2), 0.0001);
    }

    @Test
    public void testSCase2() {
        SinFunction sineFunction = new SinFunction(2, 1, 0, Math.PI);

        assertEquals(0, sineFunction.calculate(0), 0.0001);
        assertEquals(0, sineFunction.calculate(Math.PI), 0.0001);
    }

    @Test
    public void testCase3() {
        SinFunction sineFunction = new SinFunction(3, 1, 0, Math.PI);

        assertEquals(0, sineFunction.calculate(0), 0.0001);
        assertEquals(3, sineFunction.calculate(Math.PI / 2), 0.0001);
        assertEquals(0, sineFunction.calculate(Math.PI), 0.0001);
        assertEquals(-3, sineFunction.calculate(3 * Math.PI / 2), 0.0001);
    }
    @Test
    public void tastCase4() {
        SinFunction sineFunction = new SinFunction(3, 1, 0, Math.PI);

        assertThrows(IllegalArgumentException.class, () -> {sineFunction.calculate(5 * Math.PI);});
    }
}