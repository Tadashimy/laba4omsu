package org.example;

import RealFunction.RationalFunction;
import RealFunction.SinFunction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RationalFunctionTest {
    @Test
    public void testCase1() {
        RationalFunction rationalFunction = new RationalFunction(1, 2, 0, 0, -10, 10);

        assertThrows(ArithmeticException.class, () -> rationalFunction.calculate(-1));

    }
    @Test
    public void testCase2() {
        RationalFunction rationalFunction = new RationalFunction(1, 2, 3, 4, -10, 10);

        assertEquals(0.5, rationalFunction.calculate(0), 0.0001);
        assertEquals(0.42857142857142855, rationalFunction.calculate(1), 0.0001);
        assertEquals(1.0, rationalFunction.calculate(-1), 0.0001);
    }


    @Test
    public void testCase3() {
        RationalFunction rationalFunction = new RationalFunction(1, 2, 3, 4, -10, 10);

        assertEquals(0.3076923076923077, rationalFunction.calculate(-10), 0.0001);
        assertEquals(0.35294117647058826, rationalFunction.calculate(10), 0.0001);
    }
    @Test
    public void tastCase4() {
        RationalFunction rationalFunction = new RationalFunction(3, 1, 0, 2, 0, 1);

        assertThrows(IllegalArgumentException.class, () -> {rationalFunction.calculate(5);});
    }
}