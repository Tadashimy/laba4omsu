package org.example;

import Functional.RectangleIntegrationFunctional;
import RealFunction.*;
import org.junit.jupiter.api.Test;

import java.util.function.Function;
import static org.junit.jupiter.api.Assertions.*;

class RectangleIntegrationFunctionalTest {
    @Test
    public void testCase1() {
        RectangleIntegrationFunctional<LinearFunction> rectangleIntegrationFunctional =
                new RectangleIntegrationFunctional<>(0, 2);

        LinearFunction linearFunction= new LinearFunction(1, 0, 0, 2);
        double result = rectangleIntegrationFunctional.compute(linearFunction);

        assertEquals(1.99, result, 0.1);
    }

    @Test
    public void testCase2() {
        RectangleIntegrationFunctional<RealFunction> rectangleIntegrationFunctional =
                new RectangleIntegrationFunctional<>(1, -3);

        RealFunction linearFunction = new LinearFunction(2, 3, 6, 8);

        assertThrows(IllegalArgumentException.class, () -> {
            rectangleIntegrationFunctional.compute(linearFunction);
        });
    }
@Test
    public void testCase3() {
        RectangleIntegrationFunctional<LinearFunction> rectangleIntegrationFunctional =
            new RectangleIntegrationFunctional<>(4, 19);

        LinearFunction linearFunction= new LinearFunction(4, 4, 4, 19);
        double result = rectangleIntegrationFunctional.compute(linearFunction);

        assertEquals(749.55, result, 0.1);
    }

    @Test
    public void testCase4() {
        RectangleIntegrationFunctional<SinFunction> rectangleIntegrationFunctional =
                new RectangleIntegrationFunctional<>(0, 1);

        SinFunction sinFunction = new SinFunction(2, 1, 0, 1);
        double result = rectangleIntegrationFunctional.compute(sinFunction);

        assertEquals(0.91, result, 0.1);
    }


    @Test
    public void testCase5() {
        RectangleIntegrationFunctional<RationalFunction> rectangleIntegrationFunctional =
                new RectangleIntegrationFunctional<>(1, 3);

        RationalFunction rationalFunction = new RationalFunction(3, 1, 2, 1, 1, 3);
        double result = rectangleIntegrationFunctional.compute(rationalFunction);

        assertEquals(2.79, result, 0.1);
    }

    @Test
    public void testCase6() {
        RectangleIntegrationFunctional<ExponentialFunction> rectangleIntegrationFunctional =
                new RectangleIntegrationFunctional<>(2, 3);

        ExponentialFunction exponentialFunction = new ExponentialFunction(1, 0, 2, 3);
        double result = rectangleIntegrationFunctional.compute(exponentialFunction);

        assertEquals(12.7, result, 0.1);
    }
}