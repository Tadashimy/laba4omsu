package org.example;

import Functional.SumFunctional;
import RealFunction.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumFunctionalTest {

    @Test
    public void testCase1() {
        SumFunctional<LinearFunction> sumFunctional = new SumFunctional<>(0, 2);

        LinearFunction linearFunction = new LinearFunction(2, 3, 0, 2);
        double result = sumFunctional.compute(linearFunction);

        assertEquals(15, result, 0.0001);
    }

    @Test
    public void testCase2() {
        SumFunctional<RealFunction> sumFunctional = new SumFunctional<>(0, 0);

        LinearFunction linearFunction = new LinearFunction(2, 3, 0, 0);
        double result = sumFunctional.compute(linearFunction);

        assertEquals(9, result, 0.0001);
    }

    @Test
    public void testCase3() {
        SumFunctional<RealFunction> sumFunctional = new SumFunctional<>(0, 0);

        LinearFunction linearFunction = new LinearFunction(0, 0, 0, 0);
        double result = sumFunctional.compute(linearFunction);

        assertEquals(0, result, 0.0001);
    }

    @Test
    public void testCase4() {
        SumFunctional<RealFunction> sumFunctional = new SumFunctional<>(0, 4);

        SinFunction sinFunction = new SinFunction(3, 1, 0, 4);
        double result = sumFunctional.compute(sinFunction);

        assertEquals(0.45, result, 0.01);
    }

    @Test
    public void testCase5() {
        SumFunctional<RealFunction> sumFunctional = new SumFunctional<>(1, 3);

        RealFunction rationalFunction = new RationalFunction(1, 0, 1, 0, 1, 3);
        double result = sumFunctional.compute(rationalFunction);

        assertEquals(3, result, 0.01);
    }
}
