package org.example;

import RealFunction.LinearFunction;
import RealFunction.SinFunction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinearFunctionTest {

    @Test
    public void testCase1() {
        LinearFunction linearFunction = new LinearFunction(2, 3, -5, 5);

        assertEquals(1, linearFunction.calculate(-1), 0.0001);
        assertEquals(5, linearFunction.calculate(1), 0.0001);
        assertEquals(13, linearFunction.calculate(5), 0.0001);
    }

    @Test
    public void testCase2() {
        LinearFunction linearFunction = new LinearFunction(2, 3, -5, 5);

        assertEquals(-7, linearFunction.calculate(-5), 0.0001);
        assertEquals(1, linearFunction.calculate(-1), 0.0001);
        assertEquals(13, linearFunction.calculate(5), 0.0001);
    }

    @Test
    public void testCase3() {
        LinearFunction linearFunction = new LinearFunction(-2, -3, -5, 5);

        assertEquals(-1, linearFunction.calculate(-1), 0.0001);
        assertEquals(-5, linearFunction.calculate(1), 0.0001);
        assertEquals(-13, linearFunction.calculate(5), 0.0001);
    }

    @Test
    public void testCase4() {
        LinearFunction linearFunction = new LinearFunction(0, 0, 0, 0);

        assertEquals(0, linearFunction.calculate(0), 0.0001);
    }
    @Test
    public void tastCase5() {
        LinearFunction linearFunction = new LinearFunction(3, 1, 0, 1);

        assertThrows(IllegalArgumentException.class, () -> {linearFunction.calculate(5);});
    }
}